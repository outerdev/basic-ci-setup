FROM golang:latest

RUN git clone https://github.com/go-task/task && \
    cd task/cmd/task && \
    go build -o task && \
    mv task /usr/local/bin && \
    cd ../../.. && \
    rm -rf *

